# README

# API Reference Link

I have published the API from postman to the following link

https://documenter.getpostman.com/view/4863482/TVzRGdfK

# Deployment

The app was deployed to the following location using Heroku.

https://owm-rails.herokuapp.com/

# Postman Collection

To run the collection using Postman, I have included the collection file in the repo under:

`Open Weather Challenge.postman_collection.json`

Just import the collection to postman, and edit the collection variables to change the values such as api_key and units.

# API Information

## API Key

Information on how to get an API Key can be found here https://openweathermap.org/appid

## Get City Data V1

This function gets the city data from the list of names specified by the user.  It uses an API module that I created.
This module can be found in the 'app/lib/owm_api.rb' file.

Parameters:

**api** (Required): Use your Open Weather Maps API key

**units** (Optional): Defaults to "Imperial"

Possible Values:
* Imperial (Fahrenheit)
* Metric (Celcius)
* Standard (Kelvin)

**names** (Required): A list of city names separated by a `,`.

## Get City Data V2

This function performs exactly the same as the Get City Data V1 except it uses a Ruby gem I found on GitHub to make the API
calls to Open Weather Maps.

https://github.com/coderhs/ruby_open_weather_map

Parameters:

**api** (Required): Use your Open Weather Maps API key

**units** (Optional): Defaults to "Imperial"

Possible Values:
* Imperial (Fahrenheit)
* Metric (Celcius)
* Standard (Kelvin)

**names** (Required): A list of city names separated by a `,`.

## Get Session

Does not need to be called directly but can be.  When accessing either of the first two resources if a session has not
already been created the request will be redirected to this url.  This will create a session which is stored into a client cookie
and that cookie will be used to monitor a maintained session for subsequent calls.

To start a new session the cookie just needs to be removed.


# Note
 
 * Only the cities mentioned in the email are available to be queried.
