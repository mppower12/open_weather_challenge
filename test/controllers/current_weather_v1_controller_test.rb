require 'test_helper'
require 'json'
require_relative '../../app/lib/open_weather/temp_data'

class CurrentWeatherV1ControllerTest < ActionDispatch::IntegrationTest
  include OpenWeatherData
  # test "the truth" do
  #   assert true
  # end

  # test for two cities with the same temperature
  test "temperature data alpha comparison" do
    file = File.open File.expand_path("owm_test_data_alpha_prio.json", File.dirname(__FILE__))
    city_data = JSON.load file
    file.close

    compared_data = compare_returned_temp_data city_data

    puts compared_data.high_temp.city_name
    puts compared_data.low_temp.city_name
    
    # based on the test data 'dublin' should be the correct value
    assert_equal(compared_data.high_temp.city_name.downcase, "dublin")
  end

  # Test for 3 cities with different temperatures
  test "temperature data temp comparison" do 
    file = File.open File.expand_path("owm_test_data_temp_prio.json", File.dirname(__FILE__))
    city_data = JSON.load file
    file.close

    compared_data = compare_returned_temp_data city_data

    puts compared_data.high_temp.city_name
    puts compared_data.low_temp.city_name

    #based on the test data 'london' should be the high temperature and 'new york' the low
    assert compared_data.high_temp.city_name.downcase == 'london' && compared_data.low_temp.city_name.downcase == 'new york'
  end

  private
  def compare_returned_temp_data(temp_data_json)
    #initialize our data
    temp_data_array = []

    high_temp = nil

    low_temp = nil

    # check the weather data from OWM
    temp_data_json['list'].each do |city_temp|
        
        #stores the values we need
        city_name = city_temp["name"]
        city_temperature = city_temp["main"]["temp"]

        # make sure the data we want is there
        if city_name != nil && city_temperature != nil
            #create the temp data object and add it to the array
            temp_data = TemperatureData.new(city_name, city_temperature)
            temp_data_array.push(temp_data)
            
            #initialize high temp if its nil
            if high_temp == nil
                high_temp = temp_data
            end

            #initialize low temp if its nil
            if low_temp == nil
                low_temp = temp_data
            end

            # compare the high values
            if compare_values(temp_data, high_temp) == 1
                high_temp = temp_data
            end

            # compare the low values
            if compare_values(temp_data, low_temp) == -1
                low_temp = temp_data
            end
        end

    end

    # create our compared data object
    compared_data = ComparedTempData.new(high_temp, low_temp, temp_data_array)

    return compared_data
end

# if Temp1 > Temp 2 -- returns 1
# if Temp1 < Temp 2 -- returns -1
# if ties were allowed -- returns 0, but instead we check alphabetically
def compare_values(temp_1, temp_2)
    # Check if either value is nil
    if temp_1 == nil
        return -1
    end
    if temp_2 == nil
        return 1
    end
    puts 'Temps not nil'
    #puts 'temp_1 val: ' + temp_1.temperature.to_s
    #puts 'temp_2 val: ' + temp_2.temperature.to_s
    # check if temp 1 is greater than temp 2
    if temp_1.temperature > temp_2.temperature
        return 1
    elsif temp_1.temperature < temp_2.temperature
        return -1
    end

    puts 'Values match, sorting alphabetically'
    #puts 'temp_1 name: ' + temp_1.city_name
    #puts 'temp_2 name: ' + temp_2.city_name
    # if they are equal we check alphabetically
    if temp_1.city_name > temp_2.city_name
      # if temp_1 is greater that means its further down the alphabet so we return for temp 2 instead
      return -1
    else
      return 1
    end

end
end
