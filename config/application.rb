require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_mailbox/engine"
require "action_text/engine"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
#require "rails/test_unit/railtie"

require_relative "../app/lib/open_weather/data_initializer"
#require 'rake'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module OpenWeatherChallenge
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true
    
    config.middleware.use ActionDispatch::Cookies
    config.middleware.use ActionDispatch::Session::CookieStore, key: '_open_weather_challenge_session'

    config.action_dispatch.rack_cache = true

    # had to do this do to issues with loading in production
    config.autoloader = :classic

    # function thats called before initialization
    config.before_initialize do
      puts "Calling BEFORE initialization"
    end

    # Function called after initialization
    config.after_initialize do
      puts "Calling AFTER initialization"
      # automatically populate the database with the example data.
      #OpenWeatherData::OpenWeatherDataLoader.load_data
    end
  end
end
