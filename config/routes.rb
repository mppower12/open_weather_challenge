Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get "/api/v1/weather", to: "current_weather_v1#get_data_by_city_name"

  get "/api/v2/weather", to: "current_weather_v2#get_data_by_city_name"

  get "/api/v1/session", to: "session#get_session"
end
