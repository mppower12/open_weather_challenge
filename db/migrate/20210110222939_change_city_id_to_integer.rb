class ChangeCityIdToInteger < ActiveRecord::Migration[6.0]
  def change
    change_column :cities, :city_id, :integer
  end
end
