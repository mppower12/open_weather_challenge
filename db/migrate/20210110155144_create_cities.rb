class CreateCities < ActiveRecord::Migration[6.0]
  def change
    create_table :cities do |t|
      t.decimal :city_id
      t.string :name
      t.string :state
      t.string :country
      t.references :coord, null: true, foreign_key: true

      t.timestamps
    end
  end
end
