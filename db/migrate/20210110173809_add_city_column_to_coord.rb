class AddCityColumnToCoord < ActiveRecord::Migration[6.0]
  def change
    add_column :coords, :city_id, :decimal, null: true, foreign_key: true
  end
end
