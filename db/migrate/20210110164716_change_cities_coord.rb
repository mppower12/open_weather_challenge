class ChangeCitiesCoord < ActiveRecord::Migration[6.0]
  def change
      change_column :cities, :city_id, :decimal, unique: true
  end
end
