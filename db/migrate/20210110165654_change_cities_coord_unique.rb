class ChangeCitiesCoordUnique < ActiveRecord::Migration[6.0]
  def change
      add_index :cities, :city_id, unique: true
  end
end
