class CreateCoords < ActiveRecord::Migration[6.0]
  def change
    create_table :coords do |t|
      t.decimal :lon
      t.decimal :lat

      t.timestamps
    end
  end
end
