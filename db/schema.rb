# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_10_222939) do

  create_table "cities", force: :cascade do |t|
    t.integer "city_id"
    t.string "name"
    t.string "state"
    t.string "country"
    t.integer "coord_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["city_id"], name: "index_cities_on_city_id", unique: true
    t.index ["coord_id"], name: "index_cities_on_coord_id"
  end

  create_table "coords", force: :cascade do |t|
    t.decimal "lon"
    t.decimal "lat"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "city_id"
  end

  add_foreign_key "cities", "coords"
end
