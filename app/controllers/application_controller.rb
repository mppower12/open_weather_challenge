require_relative '../lib/open_weather/temp_data'
#require 'temp_data'
class ApplicationController < ActionController::API
    include OpenWeatherData
    include ActionController::Cookies
    include ActionController::Caching

    CITY_LIMIT = 10

    private
    # Checks to make sure the API key was provided
    def api_check
        api_key = params[:api]
        if api_key == nil
            # If not provided returns a 400 status with the message
            render plain: "API Key not provided.", :status => :bad_request
        end
    end

    # Checks to see if cities were provided
    def cities_check
        cities = params[:names]
        if cities == nil
            # if not provided returns a 400 status with the message
            render plain: "No cities provided", :status => :bad_request
        end
    end

    # Helper method to get the provided city names into an array
    def get_cities_array(names)
        # splits the provided names array into an array with CITY_LIMIT + 1 values
        names_array = names.split(',', CITY_LIMIT + 1)
        # after splitting the array we remove all the data in the last index leaving us with just 
        # the desired limit amount of values.
        names_array.delete_at(CITY_LIMIT)
        return names_array
    end

    #Helper method to convert names into an id string
    def convert_names_to_ids(names)
        # gets an array of names with the provided names string
        names_array = get_cities_array names
        # intialize our empty id array
        ids_array = []
        # iterate over all the names
        names_array.each do |name|
            #check and remove any trailing or leading whitespaces
            name = name.strip if name.strip
            # find the City by name
            @city = City.find_by_name(name.downcase)
            # check if we found a value
            if @city != nil
                # if we found a value add it to our id array
                puts "City not nil " + @city.name
                ids_array.push(@city.city_id.to_s)
            end
        end
        # combine the array back so we can use it as a parameter
        names_parameter = ids_array.join(",")
        puts names_parameter
        # return the name parameter
        return names_parameter
    end

    #Helper method to convert names into an id array
    def convert_names_to_id_array(names)
        # gets an array of names with the provided names string
        names_array = get_cities_array names
        # intialize our empty id array
        ids_array = []
        # iterate over all the names
        names_array.each do |name|
            #check and remove any trailing or leading whitespaces
            name = name.strip if name.strip
            # find the City by name
            @city = City.find_by_name(name.downcase)
            # check if we found a value
            if @city != nil
                # if we found a value add it to our id array
                puts "City not nil " + @city.name
                ids_array.push(@city.city_id.to_s)
            end
        end

        # returns the array of matched ids
        return ids_array
    end

    #helper method for processing temperature data and putting it into our desired model
    def compare_returned_temp_data(temp_data_json)
        #initialize our data
        temp_data_array = []

        high_temp = nil

        low_temp = nil

        # check the weather data from OWM
        temp_data_json['list'].each do |city_temp|
            
            #stores the values we need
            city_name = city_temp["name"]
            city_temperature = city_temp["main"]["temp"]

            # make sure the data we want is there
            if city_name != nil && city_temperature != nil
                #create the temp data object and add it to the array
                temp_data = TemperatureData.new(city_name, city_temperature)
                temp_data_array.push(temp_data)
                
                #initialize high temp if its nil
                if high_temp == nil
                    high_temp = temp_data
                end

                #initialize low temp if its nil
                if low_temp == nil
                    low_temp = temp_data
                end

                # compare the high values
                if compare_values(temp_data, high_temp) == 1
                    high_temp = temp_data
                end

                # compare the low values
                if compare_values(temp_data, low_temp) == -1
                    low_temp = temp_data
                end
            end

        end

        # create our compared data object
        compared_data = ComparedTempData.new(high_temp, low_temp, temp_data_array)

        return compared_data
    end

    # if Temp1 > Temp 2 -- returns 1
    # if Temp1 < Temp 2 -- returns -1
    # if ties were allowed -- returns 0, but instead we check alphabetically
    def compare_values(temp_1, temp_2)
        # Check if either value is nil
        if temp_1 == nil
            return -1
        end
        if temp_2 == nil
            return 1
        end

        # check if temp 1 is greater than temp 2
        if temp_1.temperature > temp_2.temperature
            return 1
        elsif temp_1.temperature < temp_2.temperature
            return -1
        end

        # if they are equal we check alphabetically
        if temp_1.city_name > temp_2.city_name
            # if temp_1 is greater that means its further down the alphabet so we return for temp 2 instead
            return -1
        else
            return 1
        end

    end

    # helper method to check for an active session and if one does not exist it will retrieve one
    # from the session controller.
    #
    # sessions are saved on the client with an encrypted cookie, once a session is saved requests
    # will be stored using the session id as an etag.
    def session_check
        # check the session id
        session_id = session[:session_id]
        if session_id != nil
            # debug statement
            puts "Session ID: " + session_id
        else
            # if we dont have a session redirect to session controller to get one
            # passing the full path as a parameter.
            redirect_to api_v1_session_url callback_route: request.fullpath
        end
    end
end
