require 'securerandom'

class SessionController < ApplicationController

    # gets a session if one does not exist
    def get_session
        session_cookie = cookies[:_open_weather_challenge_session]

        # checks the session cookie
        if session_cookie == nil
            # if its nil we create one with a uuid
            session[:session_id] = SecureRandom.uuid
        end

        if session_params[:callback_route] !=nil
            # redirect back to our original request route
            redirect_to session_params[:callback_route]
        end
    end


    private
    def session_params
        params.permit(:callback_route)
    end
end
