require_relative '../lib/open_weather/owm_api'
#require 'owm_api'
class CurrentWeatherV1Controller < ApplicationController
    include OpenWeatherApi

    before_action :session_check
    # all calls will require the api key
    before_action :api_check
    # getting by cities name we want to make sure cities were provided
    before_action :cities_check, only: :get_data_by_city_name

    # Gets all provided city data via their names
    def get_data_by_city_name
        # get the api key
        api = city_by_name_params[:api]
        # initialize the units
        units = "imperial"

        # if the units param is not nil we set it instead
        units = city_by_name_params[:units] if city_by_name_params[:units]

        if stale? etag: session[:session_id], public: true
            # convert queried names to ids
            queried_city_ids = convert_names_to_ids city_by_name_params[:names]
            # get our data from the current weather api
            weather_data = CurrentWeatherApi.get_cities_by_id queried_city_ids, api, units

            return_data = weather_data

            # check to see if we received something other than a 200 with the data we want.
            if weather_data['cod'] == nil
                return_data = compare_returned_temp_data weather_data
            end

            # return the resulting weather data
            render json: return_data.to_json
        end

    end

    private 
    # strongly type our paramters for security
    def city_by_name_params
        params.permit(:names, :api, :units)
    end

end
