#require_relative '../lib/open_weather/data_initializer'
#require 'data_initializer'
class CitiesController < ApplicationController

    include OpenWeatherData

    # Currently only used to load data locally from the following command -- rails runner 'CitiesController.load_data'
    # Calls the load_data class method from the OpenWeatherDataLoader.  See lib/open_weather/data_initializer.rb for more info.
    def self.load_data
        OpenWeatherDataLoader.load_data
    end
end
