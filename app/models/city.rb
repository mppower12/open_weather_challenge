class City < ApplicationRecord

    # coordinate relation
    has_one :coord, :inverse_of => :city, dependent: :destroy

    # allows for nested attributes when creating coord from city
    accepts_nested_attributes_for :coord

    # ensures city name is lower case
    before_save { self.name = name.downcase }

    # ensures the city id is unique
    validates_uniqueness_of :city_id
end
