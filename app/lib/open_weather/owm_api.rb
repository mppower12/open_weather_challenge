require 'net/http'
require 'json'

module OpenWeatherApi

    class CurrentWeatherApi

        def self.get_cities_by_id(ids, api_key, units = "imperial")
            response = Net::HTTP.get('api.openweathermap.org', '/data/2.5/group?id='+ ids +'&appid=' + api_key + '&units=' + units)
            parsed_json = JSON.parse(response)
            #puts parsed_json
            #puts parsed_json['main']['temp']
            return parsed_json
        end
    end

end