module OpenWeatherData
# This script is used to load in city data to the database.
    class OpenWeatherDataLoader
        require "json"

        def self.load_data
            puts "Calling Data Initializer"

            # open JSON file with city data
            file = File.open File.expand_path("city.list.example.json", File.dirname(__FILE__))
            # load it as JSON
            city_data = JSON.load file
            # close file now that we are done with it
            file.close

            # iterates over all JSON objects and add data to db
            # Currently doing 2 DB queries by saving @city twice.  Did this because when creating the @coord through the @city.save
            # it was not populating the coord_id column.  So I save the @coord separately, get its id and then pass it to the city model
            # for association.

            # Note: If used in a production environment using a queue with multiple threads would be best in order to speed up the process
            city_data.each do |child|
                # create the new city model
                @city = City.new(city_id: child["id"], name: child["name"].strip, state: child["state"], country: child["country"].strip)
                #save it to the db
                @city.save
                #create the new coord model with the saved city id
                @coord = Coord.new(lon: child["coord"]["lon"], lat: child["coord"]["lon"], city_id: @city.id)
                #save to db
                @coord.save
                #add the coord id to city for one-one association
                @city.coord_id = @coord.id
                #save updated city model
                @city.save
            end
        end
    end

    
end