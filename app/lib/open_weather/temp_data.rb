module OpenWeatherData

    # stores a city name and a temperature
    class TemperatureData
        attr_accessor :city_name, :temperature

        def initialize(city_name, temperature)
            @city_name = city_name
            @temperature = temperature
        end

    end

    # stores compared temperature data, high/low and all values
    class ComparedTempData

        attr_accessor :high_temp, :low_temp, :all_temps

        def initialize(high_temp, low_temp, all_temps)
            @high_temp = high_temp
            @low_temp = low_temp
            @all_temps = all_temps
        end

    end
end